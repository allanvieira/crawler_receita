# Crawler receita

### Comanos úteis
 - Bundle install
 - Aplique `chmod a+x ./bin/*`
 - Console de execução: `./bin/console`
 - Arquivo principal `lib/main.rb`
 - Arquivo que contém todo o crawler `lib/inutilizadas.rb`
 - Arquivo models e conexões `lib/models.rb`
 - Registrar ambiente `source .env.development`


### Comando
```
rake receita:iniciar[3,inutilizadas]

update execucoes set parametros='{"cnpj":"79964177000168","ano":16}'::jsonb, pendente = false, captcha_resposta = '' where token = '?';	

select (dados::jsonb->>1)::jsonb->'Ano' as ano from execucoes where token = '24';  
```

### Rodando WebService
```
rackup config.ru
```

- Fomulário de consulta Exemplo : http://localhost:9292/inutilizadas

- Iniciar uma nova execucao : http://localhost:9292/inutilizadas/consultar

- Responder uma nova execucao : http://localhost:9292/inutilizadas/responder
 
- Ler um Captcha : http://localhost:9292/inutilizadas/captcha/:id_token

### Exemplo curl

```
 curl -X GET "http://localhost:9292/inutilizadas/consultar"

 {"token":"pvhlnfbpbgvcgbdhujbciiuhgglyhnxvpgksuupcvoggmqqpxb"}

 curl -o captcha.png http://localhost:9292/inutilizadas/captcha/pvhlnfbpbgvcgbdhujbciiuhgglyhnxvpgksuupcvoggmqqpxb

 baixa o captcha

 curl -X GET "http://localhost:9292/inutilizadas/responder?token=pvhlnfbpbgvcgbdhujbciiuhgglyhnxvpgksuupcvoggmqqpxb&resposta=u6OpK6&cnpj=79964177000168&ano=16&submit=Enviar"

 retorna o json
```