Bundler.require
require "active_support"
require "active_support/core_ext"
require "./lib/models"
require "./lib/inutilizadas"
require "./lib/rotas"
require 'sucker_punch/async_syntax'


module Receita

  TIMEOUT = 20


  def self.call
    rotina = 'inutilizadas'
    token = gerar_token
    execucao = Execucao.create(:token=> token, :rotina=> rotina, :pendente=> true, :inicio=> Time.now )
    case rotina
      when 'inutilizadas' then Inutilizadas.new.async.perform(execucao)
    end
    aguardar_token(execucao)
    {
      :token => token
    }.to_json
  end

  def self.gerar_token
    tokens = Execucao.all().map{|exec| exec.token}
    token = tokens[0]
    while(tokens.include?(token))
      token = (0...50).map { ('a'..'z').to_a[rand(26)] }.join
    end
    token
  end

  def self.responder(token,resposta,cnpj,ano)
    execucao = Execucao.where(token: token).first
    unless execucao.blank?
      execucao.parametros = {:cnpj=>cnpj,:ano=>ano}.to_json
      execucao.captcha_resposta = resposta
      execucao.pendente = false
      execucao.save
      aguardar_resposta(execucao)
      if execucao.sucesso
        execucao.dados
      else
        execucao.mensagens
      end
    end
  end

  def self.aguardar_token(execucao)
    esperado = 0
    while (execucao.captcha_imagem.blank? && esperado < TIMEOUT)
      sleep 1.7
      execucao.reload
      esperado+=1
    end
  end

  def self.aguardar_resposta(execucao)
    esperado = 0
    while (execucao.sucesso.blank? && esperado < TIMEOUT)
      sleep 1.2
      execucao.reload
      esperado+=1
    end
  end



  # def aguardar_execucao()
  #   while (!(SuckerPunch::Queue.all.first.send(:pool).completed_task_count == SuckerPunch::Queue.all.first.send(:pool).scheduled_task_count) rescue false)
  #     sleep 1
  #   end
  # end

end
