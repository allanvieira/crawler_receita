module Receita

		# 
		# [ Classe que automatiza a leitura de notas inutilizadas da receita
		#   1 - Recebe uma execução identificada pelo número de um token
		#   2 - Acessa página de consulta de notas inutilizadas da receita
		#   3 - Armazena o captcha e salva no registro da execução o caminho do captcha a ser respondido
		#   4 - Fica aguardando a resposta da execução que deverá ser atualizada com o ano, cnpj, resposta do captcha e pendente = false
		#       ou responde com timeout se o tempo de espera fora maior que o tempo configurado
		#   5 - Responde o captcha da página da receita que ficou aberta enquanto aguardava resposta
		#   6 - Registra o resultado obtido 
	  #  ]
		# 
		# @author [allan]
		# 	
	class Inutilizadas
		include SuckerPunch::Job
		# Define quantidade de captchas que ficam aguardando simultaneamente
		workers 10	
		# Define quantos segundos fica aguardando o captcha ser respondido
		# Quando o captcha é respondido ou ultrapassa o tempo de espera libera a fila do suckepunch
		TIMEOUT = 120
		DOMINIO = 'http://www.nfe.fazenda.gov.br'
		URL_CONSULTA = DOMINIO + '/portal/consulta.aspx?tipoConsulta=inutilizacao&tipoConteudo=YG1QjUXR4PY='
		
		PATH_CAPTCHA = '/tmp'


				# 
				# [Método SucherPunche que centraliza o processo]
				# @param execucao [Execucao] [Registro da execução criado na base de dados identificado pelo token que se encontra com o parâmetro pendente = true]
				# 
				# @return [nil]		
		def perform(execucao)
			@execucao = execucao
	  	ler_captcha
	  	if (aguardar)
		  	responder
		  	ler_resposta
		  else
		  	@execucao.sucesso = false
		  	@execucao.captcha_sucesso = false
		  	@execucao.mensagens = 'Timeout de Resposta do Captcha'
		  end		  
		  @execucao.fim = DateTime.now
		  @execucao.save
		end


			  # 
			  # [Método que inicia o cliente Mechanize e seta os parâmetros necessários]
			  # 
			  # @return [Mechanize] [Cliente Mechanize intânciado no atributo de intância @cliente]
	  def iniciar_cliente
	    @cliente = Mechanize.new
	    @cliente.user_agent_alias = 'Linux Firefox'
	    @cliente.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
	    #@cliente.follow_redirect=false
	    @cliente
	  end
	  	

	  	  # 
	  	  # [Método responsável por definir os cookies conforme a necessidade da requisição]
	  	  # @param tipo [Text] [Tipo da requisição que esta sendo feita]
	  	  # 
	  	  # @return [Mehchanize] [Cliente Mechanize com os novos cookies] 
	  def cookies(tipo)
	  	case tipo
	  	when :inicial then
		    @cliente.request_headers = {
		    	"Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
					"Accept-Encoding" => "gzip, deflate, sdch",
					"Accept-Language" => "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4",
					"Cache-Control" => "max-age=0",
					"Connection" => "keep-alive",
					"Cookie" => "ASP.NET_SessionId=04enjdgv1jtocxzaidnw2uws",
					"Host" => "www.nfe.fazenda.gov.br",
					"Referer" => "http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=inutilizacao&tipoConteudo=YG1QjUXR4PY=",
					"Upgrade-Insecure-Requests" => 1,
					"User-Agent" => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36"
		    }
		  end
	 	end	


	 		  # 
	 		  # [Método resposável por recupera o binário do Captcha apresentado pela receita, armazenar no servidor e salvar o caminho no registro de execução]
	 		  # 
	 		  # @return [Execucao] [Registro de Execucao na base de dados com o caminho da imagem do captcha armazenada no servidor]
	  def ler_captcha
	  	iniciar_cliente
	  	cookies(:inicial)
			@pagina_formulario = @cliente.get(URL_CONSULTA) rescue nil  	
			imagem = @pagina_formulario.search("#ctl00_ContentPlaceHolder1_imgCaptcha").first.attributes["src"].value.split(',')[1]
			imagem_path = PATH_CAPTCHA+"/#{@execucao.token}.png"			  
			File.open(imagem_path, "wb") do |file|
			  file.write(Base64.decode64(imagem))
			end
			@execucao.captcha_imagem = imagem_path			
			@execucao.save
		end 


			  # 
			  # [Método responsável por aguardar até a execucao estiver com o atributo pendente = false ou então o tempo de espera for maior que o tempo de TIMEOUT]
			  # 
			  # @return [type] [description]		
	  def aguardar
	  	esperado = 0	  	
	  	while(@execucao.pendente && esperado < TIMEOUT)
	  		sleep(1)
	  		esperado+=1	  		
	  		@execucao.reload
	  	end	  		  	
	  	esperado >= TIMEOUT ? false : true
	  end	 	  
	  

	  	  # 
	  	  # [Método responsável por realizar a requisição HTTP que responde o formulário da página que ficou aguardando a resposta]
	  	  # 
	  	  # @return [Mechanize::Response] [Página de resposta da receita após responder o captcha e os parametros cnpj e ano]	  
	  def responder	  	
	  	@cliente.request_headers['Origin'] = DOMINIO
	  	@cliente.request_headers['ctl00$ContentPlaceHolder1$btnConsultar'] = 'Continuar'
	  	formulario = parametros_resposta

	  	@pagina_resposta = @cliente.post(URL_CONSULTA,formulario) rescue nil 	  	
	  end


	  	  # 
	  	  # [Método responsável por montar o formulário de resposta que será enviado na requisição HTTP de resposta do captcha]
	  	  # 
	  	  # @return [Hash] [Formulário que será utilizado na requisição HTTP]
	  def parametros_resposta
	  	@execucao.reload
	  	resposta = JSON.parse(@execucao.parametros)
	  	parametros = {}	  	
	  	@pagina_formulario.forms[0].fields.each do |parametro|
		    parametros[parametro.name] = parametro.value
		  end
		  parametros['ctl00$ContentPlaceHolder1$txtCNPJInutilizacao'] = resposta['cnpj']
		  parametros['ctl00$ContentPlaceHolder1$ddlAnoInutilizacao'] = resposta['ano']
		  parametros['ctl00$ContentPlaceHolder1$txtCaptcha'] = @execucao.captcha_resposta
		  parametros['hiddenInputToUpdateATBuffer_CommonToolkitScripts'] = 1		 
		  parametros['ctl00$ContentPlaceHolder1$btnConsultar'] = 'Continuar'
		  parametros
	  end


	  	  # 
	  	  # [Método utilizado por mapear as possíveis respostas da receita após responder o captcha]
	  	  # 
	  	  # @return [nil]
	  def ler_resposta	  	  	
	  	if @pagina_resposta.search('#ctl00_ContentPlaceHolder1_gdvInutilizacao').count > 0
				@execucao.captcha_sucesso   = true
				@execucao.sucesso  = true
				ler_inutilizadas
	  	elsif @pagina_resposta.search('#ctl00_ContentPlaceHolder1_lblResultado').text == 'Não existe registro para os dados informados.'
	  		@execucao.captcha_sucesso   = true
	  		@execucao.sucesso  = true
	  		@execucao.mensagens = 'Não existe registro para os dados informados.'
	  	elsif @pagina_resposta.search('#ctl00_ContentPlaceHolder1_lblResultado').text == 'Sistema temporariamente indisponível. Tente novamente mais tarde.'
	  		@execucao.captcha_sucesso   = true	  		
	  		@execucao.sucesso  = false
	  		@execucao.mensagens = 'Sistema temporariamente indisponível. Tente novamente mais tarde.'
	  	elsif @pagina_resposta.search('#ctl00_ContentPlaceHolder1_vdsErros ul li').count	> 0
	  		@execucao.captcha_sucesso   = false
	  		@execucao.sucesso  = false
	  		@execucao.mensagens = @pagina_resposta.search('#ctl00_ContentPlaceHolder1_vdsErros ul li').map{|erro| erro.text}	  
	  	end	  		  	
	  end


	  	  # 
	  	  # [Utilizado por ler a tabela html que contém a lista de notas inutilizadas]
	  	  # 
	  	  # @return [nil] []	  
	  def ler_inutilizadas
	  	@execucao.mensagens = "
	  		Cnpj : #{@pagina_resposta.search('#ctl00_ContentPlaceHolder1_lblCNPJValor').text}
	  		Razão Social #{@pagina_resposta.search('#ctl00_ContentPlaceHolder1_lblRazaoSocialValor').text}
	  	"
	  	dados = []
	  	cabecalho = @pagina_resposta.search('#ctl00_ContentPlaceHolder1_gdvInutilizacao tr')[0].search('th').map{ |cabecalho| cabecalho.text}
	  	@pagina_resposta.search('#ctl00_ContentPlaceHolder1_gdvInutilizacao tr').each_with_index do |linha,idx|
	  		if @pagina_resposta.search('#ctl00_ContentPlaceHolder1_gdvInutilizacao tr')[idx].search('td').count > 0
	  			nota = {}
	  			cabecalho.each_with_index do |coluna, idx_col|	  				
	  				nota[coluna] = @pagina_resposta.search('#ctl00_ContentPlaceHolder1_gdvInutilizacao tr')[idx].search('td')[idx_col].children[0].text
	  			end
	  			dados.push(nota)
	  		end	
	  	end	  	
	  	@execucao.dados = dados.to_json.to_s.strip

	  end
	end


end
