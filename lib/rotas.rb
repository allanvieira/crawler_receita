module Receita
	#
  # [Classe de Rotas]
  #
  # @author [allan]
  #
  class App < Sinatra::Base

    before do
    end


    get '/' do
    	File.read(File.join('views', 'index.html'))
    end

    get '/consultar' do
    	Receita.call
    end

    get '/responder' do
    	Receita.responder(params[:token],params[:resposta],params[:cnpj],params[:ano])
    end

    get '/captcha/:id' do
    	send_file open("/tmp/#{params['id']}.png"),
    		type: 'image/jpeg',
        disposition: 'inline'
   	end

    # ...?token=123
    #     &atualizar=[sim,nao] (opcional, padrão = sim)
    #     &cpf_cnpj=123
    #     &anp=[sim,nao] (opcional, padrão = sim)
    #     &receitaws=[sim,nao] (opcional, padrão = sim)
    #     &timeout=[inteiro] (opcional, padrão = 5)
    get '/consulta_cadastro' do

    	halt(200, Apigp.erro(['INFORME PARAMETRO CPF_CNPJ'])) if params[:cpf_cnpj].blank?
    	halt(200, Apigp.erro(['CPF_CNPJ INVÁLIDO'])) unless Apigp.cpf_cnpj_valido?(params[:cpf_cnpj])

    	cadastro = Apigp.consulta_cadastro(params)
      {
        :status => 'ok',
        :mensagens => [],
        :dados => cadastro
      }.to_json
    end

  end

end
