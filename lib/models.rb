module Receita

  # Conexão
  BD = Sequel.connect(
    {
    :adapter  => 'postgres',
    :host     => 'localhost',
    :port     => '5432',
    :database => 'downloadNfe_development',
    :user     => 'postgres',
    :password => 'postgres'
    }, :after_connect=>proc{|c| c.execute("SET application_name TO 'Receita'")}
  )

  BD.create_table? :execucoes do
    primary_key :id
    String :token
    String :rotina
    Boolean :pendente
    Json :parametros
    Json :dados
    Boolean :sucesso
    String :mensagens
    String :captcha_imagem
    String :captcha_resposta
    String :captcha_sucesso
    DateTime :inicio
    DateTime :fim
  end

  class Sequel::Model
    plugin :update_or_create
  end

  #Definição dos models
  class Execucao < Sequel::Model(BD[:execucoes])
    set_primary_key :id
  end

end
